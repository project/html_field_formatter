<?php

namespace Drupal\html_field_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'html' formatter.
 *
 * @FieldFormatter(
 *   id = "html",
 *   label = @Translation("HTML"),
 *   field_types = {
 *     "text",
 *     "text_long",
 *     "text_with_summary",
 *     "string",
 *     "string_long",
 *   }
 * )
 */
class HtmlFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'allowed_tags' => [],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['allowed_tags'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Allowed tags'),
      '#description' => $this->t('Specify allowed tags. Enter one tag per line.'),
      '#default_value' => !empty($this->getAllowedTags()) ? implode("\r\n", $this->getAllowedTags()) : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    if (!empty($this->getAllowedTags())) {
      $summary[] = $this->t('Allowed tags: %tags', ['%tags' => implode(', ', $this->getAllowedTags())]);
    }
    return $summary;
  }

  /**
   * Get allowed tags.
   *
   * @return array
   *   Returns allowed tags in array format.
   */
  private function getAllowedTags() {
    $allowed_tags = [];
    if (!empty($this->getSetting('allowed_tags'))) {
      $allowed_tags = explode("\r\n", str_replace(['<', '>'], '', $this->getSetting('allowed_tags')));
    }
    return $allowed_tags;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];

    foreach ($items as $delta => $item) {
      $value = $item->value;
      if (empty($value)) {
        $value = $item->getValue();
      }

      if (!empty($this->getAllowedTags())) {
        $elements[$delta]['#markup'] = $value;
        $elements[$delta]['#allowed_tags'] = $this->getAllowedTags();
      }
      else {
        $elements[$delta] = [
          '#children' => $value,
        ];
      }
    }

    return $elements;
  }

}
