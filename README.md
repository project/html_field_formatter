## HTML Field Formatter

This is a very simple module that enables an HTML field format for
text and string fields.

### Requirements

No special requirements at this time.

### Install

It is recommended to install via composer. See below for commands:

```bash
composer require drupal/html_field_formatter
drush en html_field_formatter
```

### Configure

* Goto _Structure -> Content Types_ and select a content type to change.
* Goto "Manage Display" page and change "Format" on desired field to "HTML".
* Configure any formatting settings as needed.
* Save changes.
* Now field should be formatted as HTML.

### Roadmap

* Add configuration settings support to formatter. Allow user to configured
supported/valid tags.

### Maintainers

* George Anderson (geoanders) - https://www.drupal.org/u/geoanders
